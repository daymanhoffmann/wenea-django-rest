Este proyecto es una prueba tecnica para la empresa wenea, el enunciado esta en
la raiz del proyecto.

 Consta de una django con docker.

La BBDD es postgrsql

Hace uso de django rest_framework y channels.
Para la documentacion de la API he añadido dos opciones:
    - swagger (http://0.0.0.0:8000/swagger/)
    - redoc (http://0.0.0.0:8000/redoc/)

La parte del bonus no está implementada. Únicamente hay una prueba en la que
mediante un websoket se listan por pantalla todos los puntos de carga de la
base de datos.

# Ejecutar los siguientes comandos por orden para arrancar el proyecto
docker-compose up --build
ctrl+c
docker-compose run web python manage.py makemigrations
docker-compose run web python manage.py migrate
docker-compose run web python manage.py createsuperuser
docker-compose up



