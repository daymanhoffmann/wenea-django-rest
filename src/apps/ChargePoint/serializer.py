from rest_framework import serializers
from src.apps.ChargePoint.models.charge_point_model import ChargePointModel


class ChargePointSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChargePointModel
        fields = '__all__'
