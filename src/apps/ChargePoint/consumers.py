from time import sleep
from src.apps.ChargePoint.models.charge_point_model import ChargePointModel
import json
from channels.generic.websocket import WebsocketConsumer


class WSConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        qs = ChargePointModel.objects.all()
        for ob in qs:
            self.send(json.dumps({'message':ob.name}))
            sleep(1)
