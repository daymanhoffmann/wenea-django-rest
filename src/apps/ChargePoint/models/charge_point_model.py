from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

CHARGE_POINT_STATUS = (
    ('r', 'ready'),
    ('c', 'charging'),
    ('w', 'waiting'),
    ('e', 'error')
)


class ChargePointModel(models.Model):
    name = models.CharField(max_length=32, null=False, unique=True)
    status = models.CharField(max_length=1, choices=CHARGE_POINT_STATUS)
    created_at = models.DateTimeField()
    deleted_at = models.DateTimeField()

    class Meta:
        db_table = 'charge_point'
        app_label = 'src'


@receiver(pre_save, sender=ChargePointModel)
def on_change(sender, instance: ChargePointModel, **kwargs):
    cps = dict(CHARGE_POINT_STATUS)
    status = cps[instance.status]
    print(status)
