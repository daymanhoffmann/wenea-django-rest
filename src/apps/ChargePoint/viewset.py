from rest_framework import viewsets
from .serializer import ChargePointSerializer
from src.apps.ChargePoint.models.charge_point_model import ChargePointModel


class ChargePointViewSet(viewsets.ModelViewSet):
    queryset = ChargePointModel.objects.all()
    serializer_class = ChargePointSerializer
