from rest_framework import routers
from .viewset import ChargePointViewSet
from .views import index
from django.urls import path, include

router = routers.SimpleRouter()
router.register('ch', ChargePointViewSet)

urlpatterns = [
    path('', index),
    path('', include(router.urls))
]
