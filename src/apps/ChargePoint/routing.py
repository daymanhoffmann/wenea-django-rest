from django.urls import path, re_path
from .consumers import WSConsumer

ws_urlpatterns = [
    path('ws/myurl/', WSConsumer.as_asgi()),
]
