from django.contrib import admin
from src.apps.ChargePoint.models.charge_point_model import ChargePointModel

admin.site.register(ChargePointModel)
