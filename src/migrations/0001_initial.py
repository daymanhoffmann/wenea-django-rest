# Generated by Django 4.1.5 on 2023-01-04 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ChargePointModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, unique=True)),
                ('status', models.CharField(choices=[('r', 'ready'), ('c', 'charging'), ('w', 'waiting'), ('e', 'error')], max_length=1)),
                ('created_at', models.DateTimeField()),
                ('deleted_at', models.DateTimeField()),
            ],
            options={
                'db_table': 'charge_point',
            },
        ),
    ]
